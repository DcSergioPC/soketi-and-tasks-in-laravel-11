<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
$count = 0;
class CountEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public int $count;
    /**
     * Create a new event instance.
     */
    public function __construct(public int $countx = 0){
        // Incrementar el contador en la caché
        // $this->count = Cache::increment('count', 1);

        // // Inicializar el contador en la caché si no existe
        // if ($this->count === 1) {
        //     Cache::put('count', 1);
        // }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new Channel('channel-count'),
        ];
    }
    public function broadcastAs() {
        return 'count-event';
    }

    public function broadcastWith() {
        // Log::info('broadcastWith called', ['count' => $this->count]);
        // $this->count += 1;
        return [
            'count' => rand(1, 100)
        ];
    }
}
