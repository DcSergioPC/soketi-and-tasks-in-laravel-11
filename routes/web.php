<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/x', function () {
    event(new App\Events\CountEvent());
    return null;
});
